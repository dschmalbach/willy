import axios from "axios";

const isProd = true;
const isProdAuth = true;

const base_auth_url = isProdAuth
  ? 'https://auth4u.bateriaswillard.com'
  : 'http://127.0.0.1:3200';
const base_url = isProd
  ? 'https://woomi.bateriaswillard.com'
  : 'http://127.0.0.1:3200';

/**
 * Envia el codigo de verificacion al usuario
 * @param {*} identification_number
 * @returns
 */
const sendkey = async (identification_number) => {
  try {
    var config = {
      method: "POST",
      url: `${base_auth_url}/auth/authenticate-send-key`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
      },
      data: {
        "identification_number": identification_number,
        "notification": "PHONE",
      }
      //, "notification": "PHONE",
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};

/**
 * login del usuario
 * @param {*} uid
 * @param {*} code
 * @returns
 */
const login = async (uid, code) => {
  try {
    var config = {
      method: "POST",
      url: `${base_auth_url}/auth/authenticate`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "wo-public-key": `wooBRyI70K26BRxAX2GEOfBAjnyPAq8yym0wrld`,
      },
      data: {
        "type": "code",
        "uid": uid,
        "code": code,
        "app": "WILLY"
      }
    };

    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * consulta las cotizaciones abiertas del usuario
 * @param {*} identification_number
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getOpenCotizations = async (identification_number, uid, token) => {
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/cotizations-open`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "identification_number": identification_number,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * consulta el detalle de una cotizacion
 * @param {*} numCot
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getCotizationDetail = async (numCot, uid, token) => {
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/cotizations-details`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numCot": numCot,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * aprueba una cotizacion
 * @param {*} numCot
 * @param {*} identification_number
 * @param {*} userName
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const aprCotizations = async (numCot, identification_number, userName, uid, token) => {
  numCot = parseInt(numCot) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/cotizacion-approve`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numCot": numCot,
        "status": "APR",
        "justif": "aprobar cotizacion",
        "identification_number": identification_number,
        "userName": userName,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * rechaza una cotizacion
 * @param {*} numCot
 * @param {*} justif
 * @param {*} identification_number
 * @param {*} userName
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const rechCotizations = async (numCot, justif, identification_number, userName, uid, token) => {
  numCot = parseInt(numCot) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/cotizacion-approve`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numCot": numCot,
        "status": "RCH",
        "justif": justif,
        "identification_number": identification_number,
        "userName": userName,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * consulta las requisiciones abiertas del usuario
 * @param {*} identification_number
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getOpenRequisitions = async (identification_number, uid, token) => {
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-open`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "identification_number": identification_number,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }

};
/**
 * consulta el detalle de una requisicion
 * @param {*} numReq
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getRequisitionDetail = async (numReq, uid, token) => {
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-details`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numReq": numReq,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * consulta el detalle de una linea de requisicion
 * @param {*} numReq
 * @param {*} line
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getRequisitionLineDetail = async (numReq, line, uid, token) => {
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-line-details`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numReq": numReq,
        "line": line,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};

/**
 * aprueba una linea de requisicion
 * @param {*} numReq
 * @param {*} line
 * @param {*} uid
 * @param {*} token
 * @returns
 */

const aprRequisitionLine = async (numReq, line, uid, token) => {
  numReq = parseInt(numReq) || 0;
  line = parseInt(line) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-approve-line`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numReq": numReq,
        "line": line,
        "action": "APR",
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};

/**
 * rechaza una linea de requisicion
 * @param {*} numReq
 * @param {*} line
 * @param {*} uid
 * @param {*} token
 * @returns
 */

const rechRequisitionLine = async (numReq, line, uid, token) => {
  numReq = parseInt(numReq) || 0;
  line = parseInt(line) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-approve-line`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numReq": numReq,
        "line": line,
        "action": "RCH",
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * aprueba una requisicion
 * @param {*} numReq
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const aprRequisition = async (numReq, uid, token) => {
  numReq = parseInt(numReq) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-approve`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numReq": numReq,
        "action": "APR",
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * rechaza una requisicion
 * @param {*} numReq
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const rechRequisition = async (numReq, uid, token) => {
  numReq = parseInt(numReq) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-approve`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numReq": numReq,
        "action": "RCH",
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * cierra una requisicion
 * @param {*} numReq
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const closeRequisition = async (numReq, uid, token) => {
  numReq = parseInt(numReq) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/requisitions-approve`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "numReq": numReq,
        "action": "CLOSE",
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * consulta las facturas abiertas del usuario
 * @param {*} identification_number
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getOpenInvoices = async (identification_number, uid, token) => {
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/invoices-open`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "identification_number": identification_number,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * consulta el detalle de una factura
 * @param {*} id_doc
 * @param {*} number
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getInvoiceDetails = async (id_doc, number, uid, token) => {
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/invoice-details`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "id_doc": id_doc,
        "number": number,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * consulta el detalle de un centro de costo
 * @param {*} number
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const getCostCenterInfo = async (number, uid, token) => {
  number = parseInt(number) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/cost-center-info`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "number": number,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * aprueba una factura
 * @param {*} id_doc
 * @param {*} number
 * @param {*} userName
 * @param {*} costNumber
 * @param {*} comment
 * @param {*} calificacion
 * @param {*} justificacionCalificacion
 * @param {*} numOc
 * @param {*} provider
 * @param {*} uid
 * @param {*} token
 * @returns
*/
const aprInvoice = async (id_doc, number, userName, costNumber, comment, calificacion, justificacionCalificacion, numOc, provider, typeasoc, uid, token) => {
  id_doc = parseInt(id_doc) || 0;
  costNumber = parseInt(costNumber) || 0;
  numOc = parseInt(numOc) || null;
  calificacion = parseInt(calificacion) || "";
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/invoice-approve`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "id_doc": id_doc,
        "number": number,
        "userName": userName,
        "type": typeasoc,
        "costNumber": costNumber,
        "comment": comment,
        "calificacion": calificacion,
        "justificacionCalificacion": justificacionCalificacion,
        "numOc": numOc,
        "provider": provider,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
/**
 * rechaza una factura
 * @param {*} id_doc
 * @param {*} cauRech
 * @param {*} justRech
 * @param {*} userRech
 * @param {*} userRechN
 * @param {*} uid
 * @param {*} token
 * @returns
 */
const rechInvoice = async (id_doc, cauRech, justRech, userRech, userRechN, uid, token) => {
  id_doc = parseInt(id_doc) || 0;
  try {
    var config = {
      method: "POST",
      url: `${base_url}/bcenter/invoice-reject`,
      headers: {
        "idplataforma": `4`,
        "imei": `bot605t22c7225n66d256262`,
        "Content-Type": `application/json`,
        "woo-token": token,
        "woo-uid": uid,
      },
      data: {
        "id_doc": id_doc,
        "cauRech": cauRech,
        "justRech": justRech,
        "userRech": userRech,
        "userRechN": userRechN,
        //"motorBD": "test92"
      }
    };
    const response = await axios(config);
    return response.data;
  } catch (e) {
    console.error(e);
    return e;
  }
};
export {
  sendkey,
  login,
  getOpenCotizations,
  getCotizationDetail,
  aprCotizations,
  rechCotizations,
  getOpenRequisitions,
  getRequisitionDetail,
  getRequisitionLineDetail,
  aprRequisitionLine,
  rechRequisitionLine,
  aprRequisition,
  rechRequisition,
  closeRequisition,
  getOpenInvoices,
  getInvoiceDetails,
  aprInvoice,
  rechInvoice,
  getCostCenterInfo,
};