async function formatCotizacionDetails(cotizacion) {
    let details = `Detalle de Cotización (${cotizacion.items.length} Items)\n`;
    details += `*Cotización:* ${cotizacion.number}\n`;
    details += `*Fecha:* ${cotizacion.date}\n`;
    details += `*Usuario:* ${cotizacion.user}\n`;
    details += `*Obs:* ${cotizacion.observation}\n`;
    details += "-----------------------------------------\n";

    cotizacion.items.forEach((item, index) => {
        details += `*Item #${index + 1}:*\n`;
        details += `*Tipo:* ${item.type}\n`;
        details += `*Descrip:* ${item.description}\n`;
        details += `*Adjunto:* \n `;

        let adjuntos = item.attachments.map((adj, index) => {
            return `*${index + 1}*. ${adj}\n`;
        });
        let adjuntos_ = item.attachments.length > 0 ? `${adjuntos.join('')}\n` : ' -- sin adjuntos --\n\n';
        details += adjuntos_;

    });
    details += "\n";

    details += `${numeroAEmoji(1)} ✅ Aceptar\n`;
    details += `${numeroAEmoji(2)} ❌ Rechazar\n`;
    details += `${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior\n`;
    details += "*Elija una opción:*\n";
    return details;
}

async function formatRequisitionDetails(requisition) {
    let details = `Detalle de Requisicion (${requisition.items.length} Items)\n`;
    details += `*Requisicion:* ${requisition.number}\n`;
    details += `*Valor total Requisicion:* ${requisition.value_total}\n`;
    details += `*Fecha:* ${requisition.date}\n`;
    details += `*Usuario:* ${requisition.user}\n`;
    details += `*Obs:* ${requisition.observation}\n`;
    details += "-----------------------------------------\n";

    requisition.items.forEach((item, index) => {
        details += `${numeroAEmoji(index + 1)} *Item:* ${item.item}\n`;
        details += `*Descrip:* ${item.description}\n`;
        details += `*Linea:* ${item.line}\n`;
        details += `*Cant:* ${item.quantity}\n`;
        details += `*Total:* ${item.total}\n\n`;
    });
    details += "\n";

    let aceptar = requisition.items.length + 1;
    let rechazar = requisition.items.length + 2;
    let cerrar = requisition.items.length + 3;

    details += `${numeroAEmoji(aceptar)} ✅ Aceptar Todo\n`;
    details += `${numeroAEmoji(rechazar)} ❌ Rechazar Todo\n`;
    details += `${numeroAEmoji(cerrar)} 📤 Cerrar Requisicion\n`;
    details += `${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior\n\n`;
    details += "*Elija una opción:*\n";
    return details;
}

async function formatRequisitionLineDetails(requisition) {
    let details = `*Requisicion:* ${requisition.number}\n`;
    details += `Detalle del item (${requisition.line})\n`;
    details += `*Cantidad:* ${requisition.quantity}\n`;
    details += `*Valor total Item:* ${requisition.total}\n`;
    details += `*Fecha:* ${requisition.date}\n`;
    details += `*Usuario:* ${requisition.user}\n`;
    details += `*Obs:* ${requisition.observation}\n`;
    details += "-----------------------------------------\n";

    let aceptar = 1;
    let rechazar = 2;

    details += `${numeroAEmoji(aceptar)} ✅ Aceptar\n`;
    details += `${numeroAEmoji(rechazar)} ❌ Rechazar\n`;
    details += `${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior\n\n`;
    details += "*Elija una opción:*\n";
    return details;
}

async function formatInvoicesDetails(invoice) {
    let details = `Detalle de Factura (${invoice.number})\n`;
    details += `*Factura:* ${invoice.number}\n`;
    details += `*Orden de Compra:* ${invoice.numoc === null ? ' - ' : invoice.numoc}\n`;
    details += `*Fecha:* ${invoice.date}\n`;
    details += `*Proveedor:* ${invoice.user}\n`;
    details += `*Obs:* ${invoice.observation}\n`;

    details += "-----------------------------------------\n";
    details += `*Anexos:* \n`;
    let anexos = invoice.attachments.map((adj, index) => {
        return `*${index + 1}*. ${adj}\n`;
    });
    let anexos_ = invoice.attachments.length > 0 ? `${anexos.join('')}\n` : ' -- sin anexos --\n\n';
    details += anexos_;

    details += "\n";

    details += `${numeroAEmoji(1)} ✅ Aceptar\n`;
    details += `${numeroAEmoji(2)} ❌ Rechazar\n`;
    details += `${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior\n`;
    details += "*Elija una opción:*\n";
    return details;
}

function numeroAEmoji(numero) {
    return numero.toString().split('').map(digitoAEmoji).join('') + '️.';
}
function digitoAEmoji(digit) {
    const emojiMap = {
        '0': '0️⃣', '1': '1️⃣', '2': '2️⃣', '3': '3️⃣', '4': '4️⃣',
        '5': '5️⃣', '6': '6️⃣', '7': '7️⃣', '8': '8️⃣', '9': '9️⃣'
    };
    return emojiMap[digit] || digit;
}
export { formatCotizacionDetails, formatRequisitionDetails, formatRequisitionLineDetails, formatInvoicesDetails, numeroAEmoji };