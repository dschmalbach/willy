FROM node:18-bullseye as bot
RUN mkdir -p /home/node/app
#setear directorio HOME
WORKDIR /home/node/app
COPY package*.json ./
RUN npm i
COPY . .
ARG RAILWAY_STATIC_URL
ARG PUBLIC_URL
ARG PORT
CMD ["npm", "start"]
