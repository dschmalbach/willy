/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowFacturaFin } from "./flowFacturaFin.js";
/**
 * Funciones
 */
import { rechInvoice } from "../api/woomi.service.js";
/**
 * Constantes Flow
 */
const flowFacturaRechDet = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer([
        "*digite justificación de rechazo:*",
    ], { capture: true }, async (ctx, ctxFn) => {

        if (ctx.body.length < 3) {
            return await ctxFn.fallBack();
        }

        const invcauRech = ctxFn.state.get('invcauRech');
        const justRech = ctx.body;
        const userRech = ctxFn.state.get('userName');
        const userRechN = ctxFn.state.get('nombre');
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');

        try {
            const res = await rechInvoice(invItemSelected.id, invcauRech, justRech, userRech, userRechN, uid, token);

            if (res.code === 200) {
                await ctxFn.flowDynamic(`La factura #${invItemSelected.number}. ha sido rechazada.`);
            } else {
                await ctxFn.flowDynamic(`${res.messages.error}`);
            }
            return await ctxFn.gotoFlow(flowFacturaFin);
        } catch (error) {
            console.log(error);
            await ctxFn.flowDynamic(`Error al rechazar la factura #${invItemSelected.number}.`);
            return await ctxFn.gotoFlow(flowFacturaFin);
        }
    });
export { flowFacturaRechDet };