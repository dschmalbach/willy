/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowRequision } from "./flowRequision.js";
import { flowRequisionLineDet } from "./flowRequisionLineDet.js";
import { flowRequisionFin } from "./flowRequisionFin.js";
/**
 * Funciones
 */
import { getRequisitionDetail, aprRequisition, rechRequisition, closeRequisition } from "../api/woomi.service.js";
import { formatRequisitionDetails, numeroAEmoji } from "../api/utils.js";
/**
 * Constantes Flow
 */
const flowRequisionDet = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer('procesando....')
    .addAction(async (_, ctxFn) => {
        const reqSelected = parseFloat(ctxFn.state.get('reqSelected')) || 0;
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');
        try {
            const res = await getRequisitionDetail(reqSelected, uid, token);
            if (res.code === 200) {
                //console.log('data:', res.data);
                let msm = await formatRequisitionDetails(res.data);

                await ctxFn.state.update({
                    reqItemsLine: res.data.items || [],
                })
                return await ctxFn.flowDynamic(msm)
            } else {
                await ctxFn.state.update({
                    reqItemsLine: [],
                })
                await ctxFn.flowDynamic(`${res.messages.error}`);
                return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
            }
        } catch (error) {
            console.log('error:', error);
            await ctxFn.state.update({
                reqItemsLine: [],
            })
            await ctxFn.flowDynamic(`Error al consultar la Solicitud de requisicion #${reqSelected}.`);
            return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
        }

    })
    .addAction({ capture: true }, async (ctx, ctxFn) => {
        const reqItemsLine = ctxFn.state.get('reqItemsLine') || [];
        const reqSelected = ctxFn.state.get('reqSelected');
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');

        if (isNaN(ctx?.body)) {
            return await ctxFn.gotoFlow(flowRequisionDet);
        }
        //si el número digitado es 0, salimos del flujo
        if (parseInt(ctx.body) === 0) {
            return await ctxFn.gotoFlow(flowRequision);
        }

        if (parseInt(ctx.body) > reqItemsLine.length + 3) {
            return await ctxFn.gotoFlow(flowRequisionDet);
        } else if (parseInt(ctx.body) === reqItemsLine.length + 1) {
            try {
                const res = await aprRequisition(reqSelected, uid, token);

                if (res.code === 200) {
                    await ctxFn.flowDynamic(`la Solicitud de requisicion #${reqSelected} ha sido aprobada Exitosmante.`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                } else {
                    await ctxFn.flowDynamic(`${res.messages.error}`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                }
            } catch (error) {
                console.log('error:', error);
                await ctxFn.flowDynamic(`Error al aprobar la Solicitud de requisicion #${reqSelected}.`);
                return await ctxFn.gotoFlow(flowRequisionFin);
            }

        } else if (parseInt(ctx.body) === reqItemsLine.length + 2) {
            const res = await rechRequisition(reqSelected, uid, token);
            try {
                if (res.code === 200) {
                    await ctxFn.flowDynamic(`la Solicitud de requisicion #${reqSelected} ha sido rechazada Exitosmante.`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                } else {
                    await ctxFn.flowDynamic(`${res.messages.error}`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                }
            } catch (error) {
                console.log('error:', error);
                await ctxFn.flowDynamic(`Error al rechazar la Solicitud de requisicion #${reqSelected}.`);
                return await ctxFn.gotoFlow(flowRequisionFin);
            }

        } else if (parseInt(ctx.body) === reqItemsLine.length + 3) {
            try {
                const res = await closeRequisition(reqSelected, uid, token);
                if (res.code === 200) {
                    await ctxFn.flowDynamic(`la Solicitud de requisicion #${reqSelected} ha sido cerrada Exitosmante.`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                } else {
                    await ctxFn.flowDynamic(`${res.messages.error}`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                }
            } catch (error) {
                console.log('error:', error);
                await ctxFn.flowDynamic(`Error al cerrar la Solicitud de requisicion #${reqSelected}.`);
                return await ctxFn.gotoFlow(flowRequisionFin);
            }
        } else {
            const itemLine = reqItemsLine[parseInt(ctx.body) - 1];
            if (!itemLine) {
                await ctxFn.state.update({
                    reqItemsLine: [],
                });
                ctxFn.flowDynamic(`El número digitado no es válido, por favor intenta nuevamente.`);
                return await ctxFn.gotoFlow(flowRequisionDet);
            }
            await ctxFn.state.update({
                reqLinSelected: itemLine.line,
            });
            return await ctxFn.gotoFlow(flowRequisionLineDet);
        }
    });
export { flowRequisionDet };