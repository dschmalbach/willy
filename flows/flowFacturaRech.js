/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowFacturaDet } from "./flowFacturaDet.js";
import { flowFacturaRechDet } from "./flowFacturaRechDet.js";
/**
 * Funciones
 */
import { numeroAEmoji } from "../api/utils.js";
/**
 * Constantes Flow
 */
const flowFacturaRech = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer([
        "*porfavor indique una razon de rechazo:*",
        `${numeroAEmoji(1)} NO CONFORME`,
        `${numeroAEmoji(2)} NO SOY APROBADOR`,
        `${numeroAEmoji(0)} ⬅️ ⁠Volver al detalle de la factura`,
    ], { capture: true }, async (ctx, ctxFn) => {

        if (isNaN(ctx?.body)) {
            return await ctxFn.fallBack();
        }

        if (parseInt(ctx.body) === 0) {
            return await ctxFn.gotoFlow(flowFacturaDet);
        }

        if (parseInt(ctx?.body) > 2) {
            return await ctxFn.fallBack();
        }

        if (parseInt(ctx?.body) === 1) {
            await ctxFn.state.update({
                invcauRech: 'NO CONFORME',
            });
        } else if (parseInt(ctx?.body) === 2) {
            await ctxFn.state.update({
                invcauRech: 'NO SOY APROBADOR',
            });
        }

        return await ctxFn.gotoFlow(flowFacturaRechDet);

    });
export { flowFacturaRech };