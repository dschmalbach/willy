/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowMenu } from "./flowMenu.js";
/**
 * Funciones
 */
import { numeroAEmoji } from "../api/utils.js";
/**
 * Constantes Flow
 */
const flowOc = bot.addKeyword(bot.EVENTS.ACTION)
  .addAnswer(
    ['Listado de Ordenes de Compra',
      'esta opcion no esta disponible',
      `${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior`],
    {
      capture: true,
    },
    async (ctx, ctxFn) => {
      if (isNaN(ctx?.body)) {
        return await ctxFn.fallBack();
      }
      switch (parseInt(ctx.body)) {
        case 0:
          return ctxFn.gotoFlow(flowMenu);
        default:
          return await ctxFn.fallBack();
      }
    });
export { flowOc };
