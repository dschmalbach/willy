/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Constantes Flow
 */
const flowFinalizar = bot
    .addKeyword(bot.EVENTS.ACTION)
    .addAnswer("Gracias por usar nuestro servicio.. 👋");
export { flowFinalizar };