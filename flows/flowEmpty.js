/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Constantes Flow
 */
const flowEmpty = bot
    .addKeyword(bot.EVENTS.ACTION)
    .addAnswer("No te he entendido!", null, async (_, { gotoFlow }) => {
        return gotoFlow(flowPrincipal);
    });
export { flowEmpty };
