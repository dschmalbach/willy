/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowCotizacion } from "./flowCotizacion.js";
import { flowCotizacionFin } from "./flowCotizacionFin.js";
import { flowCotizacionRech } from "./flowCotizacionRech.js";
/**
 * Funciones
 */
import { getCotizationDetail, aprCotizations } from "../api/woomi.service.js";
import { formatCotizacionDetails, numeroAEmoji } from "../api/utils.js";
/**
 * Constantes Flow
 */
const flowCotizacionDet = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer('procesando....')
    .addAction(async (_, ctxFn) => {
        const cotSelected = parseFloat(ctxFn.state.get('cotSelected')) || 0;
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');
        try {
            const res = await getCotizationDetail(cotSelected, uid, token);
            if (res.code === 200) {
                //console.log('data:', res.data);
                let msm = await formatCotizacionDetails(res.data);
                return await ctxFn.flowDynamic(msm)
            } else {
                await ctxFn.flowDynamic(`${res.messages.error}`);
                return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
            }
        } catch (error) {
            console.log(error);
            await ctxFn.flowDynamic(`Error al consultar la Solicitud de cotizacion #${cotSelected}.`);
            return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
        }

    })
    .addAction({ capture: true }, async (ctx, ctxFn) => {
        const cotSelected = ctxFn.state.get('cotSelected');
        if (isNaN(ctx?.body)) {
            return await ctxFn.gotoFlow(flowCotizacionDet);
        }
        //si el número digitado es 0, salimos del flujo
        if (parseInt(ctx.body) === 0) {
            return await ctxFn.gotoFlow(flowCotizacion);
        }

        if (parseInt(ctx.body) === 1) {

            const identification_number = parseFloat(ctxFn.state.get('identification_number')) || 0;
            const nombre = ctxFn.state.get('nombre');
            const uid = ctxFn.state.get('uid');
            const token = ctxFn.state.get('token');

            try {
                const res = await aprCotizations(cotSelected, identification_number, nombre, uid, token);

                if (res.code === 200) {
                    await ctxFn.flowDynamic(`la Solicitud de cotizacion #${cotSelected} ha sido aprobada Exitosmante.`);
                } else {
                    await ctxFn.flowDynamic(`${res.messages.error}`);
                }
                return await ctxFn.gotoFlow(flowCotizacionFin);
            } catch (error) {
                console.log(error);
                await ctxFn.flowDynamic(`Error al aprobar la Solicitud de cotizacion #${cotSelected}.`);
                return await ctxFn.gotoFlow(flowCotizacionFin);
            }

        }

        if (parseInt(ctx.body) === 2) {
            await ctxFn.flowDynamic(`Usted ha decidido rechazar la Solicitud #${cotSelected},`);
            return await ctxFn.gotoFlow(flowCotizacionRech);
        }
        if (parseInt(ctx.body) > 2) {
            return await ctxFn.gotoFlow(flowCotizacionDet);
        }
    });
export { flowCotizacionDet };