/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowRequision } from "./flowRequision.js";
import { flowFinalizar } from "./flowFinalizar.js";
/**
 * Funciones
 */
import { numeroAEmoji } from "../api/utils.js";

/**
 * Constantes Flow
 */
const flowRequisionFin = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer([
        "*Elija una opción:*",
        `${numeroAEmoji(1)} Finalizar`,
        `${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior`
    ], { capture: true }, async (ctx, ctxFn) => {

        if (isNaN(ctx?.body)) {
            return await ctxFn.fallBack();
        }
        switch (parseInt(ctx.body)) {
            case 0:
                return ctxFn.gotoFlow(flowRequision);
            case 1:
                return ctxFn.gotoFlow(flowFinalizar);
            default:
                return await ctxFn.fallBack();
        }
    });
export { flowRequisionFin };