/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Constantes Flow
 */
const flowInactividad = bot
    .addKeyword(bot.EVENTS.ACTION)
    .addAnswer(['❌ Se canceló por inactividad, gracias por usar nuestros servicios.'])
    .addAction((_, { endFlow }) => {
        console.log('Inactividad');
        return endFlow()
    })
export { flowInactividad };
