/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowCotizacionDet } from "./flowCotizacionDet.js";
import { flowCotizacionFin } from "./flowCotizacionFin.js";
/**
 * Funciones
 */
import { numeroAEmoji } from "../api/utils.js";
import { rechCotizations } from "../api/woomi.service.js";
/**
 * Constantes Flow
 */
const flowCotizacionRech = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer([
        "*porfavor indique una razon de rechazo:*",
        `${numeroAEmoji(1)} Registro equivocado`,
        `${numeroAEmoji(2)} NO se va Solicitar`,
        `${numeroAEmoji(3)} Solicitud repetida`,
        `${numeroAEmoji(4)} otra`,
        `${numeroAEmoji(0)} ⬅️ ⁠Volver al detalle de la Solicitud`
    ], { capture: true }, async (ctx, ctxFn) => {

        const cotSelected = ctxFn.state.get('cotSelected');

        if (isNaN(ctx?.body)) {
            return await ctxFn.fallBack();
        }

        if (parseInt(ctx.body) === 0) {
            return await ctxFn.gotoFlow(flowCotizacionDet);
        }

        if (parseInt(ctx?.body) > 4) {
            return await ctxFn.fallBack();
        }

        let flowDynamic_ = '';
        let justif = '';

        switch (parseInt(ctx.body)) {
            case 1:
                flowDynamic_ = `Confirmamos que la Solicitud #${cotSelected}. ha sido Anulada con la Justificacion: *Registro equivocado*`;
                justif = 'Registro equivocado';
                break;
            case 2:
                flowDynamic_ = `Confirmamos que la Solicitud #${cotSelected}. ha sido Anulada con la Justificacion: *NO se va Solicitar*`;
                justif = 'NO se va Solicitar';
                break;
            case 3:
                flowDynamic_ = `Confirmamos que la Solicitud #${cotSelected}. ha sido Anulada con la Justificacion: *Solicitud repetida*`;
                justif = 'Solicitud repetida';
                break;
            case 4:
                flowDynamic_ = `Confirmamos que la Solicitud #${cotSelected}. ha sido Anulada con la Justificacion: *otra*`;
                justif = 'otra';
                break;
            default:
                return await ctxFn.fallBack();
        }

        const identification_number = parseFloat(ctxFn.state.get('identification_number')) || 0;
        const nombre = ctxFn.state.get('nombre');
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');

        try {
            const res = await rechCotizations(cotSelected, justif, identification_number, nombre, uid, token);

            if (res.code === 200) {
                await ctxFn.flowDynamic(flowDynamic_);
            } else {
                await ctxFn.flowDynamic(`${res.messages.error}`);
            }
            return await ctxFn.gotoFlow(flowCotizacionFin);
        } catch (error) {
            console.log(error);
            await ctxFn.flowDynamic(`Error al rechazar la Solicitud de cotizacion #${cotSelected}.`);
            return await ctxFn.gotoFlow(flowCotizacionFin);
        }
    });
export { flowCotizacionRech };