import bot from "@bot-whatsapp/bot";

import { flowSendCode } from "./flowSendCode.js";

const flowPrincipal = bot.addKeyword(bot.EVENTS.WELCOME)
  .addAnswer([
    "Bienvenido soy *Wilbot* 🤖",
    "tu asistente virtual",
    "*Baterias Willard S.A*",
    "",
    "Al continuar en este chat aceptas los términos y condiciones de nuestra política de tratamiento de datos, privacidad, seguridad y uso de la información, esta la puedes consultar en *www.bateriaswillard.com*."
  ],
    null,
    (__, ctxFn) => {
      //console.log("ctxFn:::", ctxFn);
      ctxFn.gotoFlow(flowSendCode);
    });
export { flowPrincipal };