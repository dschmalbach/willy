/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowMenu } from "./flowMenu.js";
import { flowRequisionDet } from "./flowRequisionDet.js";
/**
 * Funciones
 */
import { getOpenRequisitions } from "../api/woomi.service.js";
import { numeroAEmoji } from "../api/utils.js";

/**
 * Constantes Flow
 */
const flowRequision = bot.addKeyword(bot.EVENTS.ACTION)
  .addAnswer(['Estamos procesando su solicitud, por favor espere....'])
  .addAction(async (_, ctxFn) => {
    const myState = ctxFn.state.getMyState();
    console.log('myState:', myState);
    const identification_number = parseFloat(ctxFn.state.get('identification_number')) || 0;
    const uid = ctxFn.state.get('uid');
    const token = ctxFn.state.get('token');

    try {
      const res = await getOpenRequisitions(identification_number, uid, token);
      if (res.code === 200) {
        await ctxFn.state.update({
          reqItems: res.data,
        });
        let dataIt = res.data || [];
        //console.log('res:', res);
        let listItems = dataIt.map((item, index) =>
          `${numeroAEmoji(index + 1)} Req: ${item.number}, Detalle: ${item.description}`
        ).join('\n');
        listItems = `${listItems}\n${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior`;
        return await ctxFn.flowDynamic(`*Listado de Requisiones:*\n${listItems}\n\n\n*para ver el detalle digite una opción:*`);
      } else if (res.code === 404) {
        await ctxFn.flowDynamic(`No se encontraron Requisiones abiertas para este usuario, por favor intenta más tarde.`);
        return await ctxFn.flowDynamic(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
      } else {
        await ctxFn.flowDynamic(`${res.messages.error}`);
        return await ctxFn.flowDynamic(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
      }
    } catch (error) {
      console.log('error:', error);
      await ctxFn.flowDynamic(`Error al consultar las Requisiones abiertas para el usuario.`);
      return await ctxFn.flowDynamic(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
    }

  })
  .addAction({ capture: true }, async (ctx, ctxFn) => {
    if (isNaN(ctx?.body)) {
      await ctxFn.state.update({
        reqItems: [],
      });
      return await ctxFn.gotoFlow(flowRequision);
    }
    //si el número digitado es 0, salimos del flujo
    if (parseInt(ctx.body) === 0) {
      await ctxFn.state.update({
        reqItems: [],
      });
      return await ctxFn.gotoFlow(flowMenu);
    }
    //validamos que el número digitado sea correcto y que exista en el listado
    const reqItems = ctxFn.state.get('reqItems');
    const item = reqItems[parseInt(ctx.body) - 1];
    if (!item) {
      await ctxFn.state.update({
        reqItems: [],
      });
      ctxFn.flowDynamic(`El número digitado no es válido, por favor intenta nuevamente.`);
      return await ctxFn.gotoFlow(flowRequision);
    }

    await ctxFn.state.update({
      reqSelected: item.number,
    });
    //const myState = ctxFn.state.getMyState();
    //console.log('myState:', myState);
    return ctxFn.gotoFlow(flowRequisionDet);
  });
export { flowRequision };
