/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowRequision } from "./flowRequision.js";
import { flowCotizacion } from "./flowCotizacion.js";
import { flowFactura } from "./flowFactura.js";
import { flowOc } from "./flowOc.js";
/**
 * Funciones
 */
import { numeroAEmoji } from "../api/utils.js";
/**
 * Constantes Flow
 */
const flowMenu = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer(["*Elija una opción:*",
        `${numeroAEmoji(1)} Gestionar Cotizaciones`,
        `${numeroAEmoji(2)} Gestionar Requisiciones`,
        `${numeroAEmoji(3)} Gestionar Facturas`,
        `${numeroAEmoji(4)} Gestionar Ordenes de Compra`,
        `${numeroAEmoji(0)} Salir`], { capture: true }, async (ctx, ctxFn) => {

            if (isNaN(ctx?.body)) {
                return await ctxFn.fallBack();
            }
            switch (parseInt(ctx.body)) {
                case 1:
                    return ctxFn.gotoFlow(flowCotizacion);
                case 2:
                    return ctxFn.gotoFlow(flowRequision);
                case 3:
                    return ctxFn.gotoFlow(flowFactura);
                case 4:
                    return ctxFn.gotoFlow(flowOc);
                case 0:
                    return ctxFn.endFlow("Gracias por usar nuestro servicio.. 👋");
                default:
                    return await ctxFn.fallBack();
            }

        });
export { flowMenu };
