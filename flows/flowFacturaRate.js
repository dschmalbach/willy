/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowFacturaRateDet } from "./flowFacturaRateDet.js";
/**
 * Funciones
 */

/**
 * Constantes Flow
 */
const flowFacturaRate = bot.addKeyword(bot.EVENTS.ACTION)
  .addAnswer([
    "Sr. Usuario califique con una puntuación de 1 a 10 su nivel de satisfacción con este servicio. (siendo 1 la calificación más baja y 10 la más alta).",
    "*digite una opción:*",
  ], { capture: true }, async (ctx, ctxFn) => {

    if (isNaN(ctx?.body)) {
      await ctxFn.state.update({
        invRate: 0,
        invComment: '',
      });
      return await ctxFn.fallBack();
    }

    if (parseInt(ctx.body) > 0 && parseInt(ctx.body) < 11) {
      await ctxFn.state.update({
        invRate: parseInt(ctx.body),
        invComment: '',
      });
      return await ctxFn.gotoFlow(flowFacturaRateDet);
    }

    if (parseInt(ctx?.body) > 11) {
      await ctxFn.state.update({
        invRate: 0,
        invComment: '',
      });
      return await ctxFn.fallBack();
    }

  });
export { flowFacturaRate };