/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowRequisionDet } from "./flowRequisionDet.js";
import { flowRequisionFin } from "./flowRequisionFin.js";
/**
 * Funciones
 */
import { getRequisitionLineDetail, aprRequisitionLine, rechRequisitionLine } from "../api/woomi.service.js";
import { numeroAEmoji, formatRequisitionLineDetails } from "../api/utils.js";
/**
 * Constantes Flow
 */
const flowRequisionLineDet = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer('procesando....')
    .addAction(async (_, ctxFn) => {
        const reqSelected = parseFloat(ctxFn.state.get('reqSelected')) || 0;
        const reqLinSelected = parseFloat(ctxFn.state.get('reqLinSelected')) || 0;
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');
        try {
            const res = await getRequisitionLineDetail(reqSelected, reqLinSelected, uid, token);
            if (res.code === 200) {
                //console.log('data:', res.data);
                let msm = await formatRequisitionLineDetails(res.data);
                return await ctxFn.flowDynamic(msm)
            } else {
                await ctxFn.flowDynamic(`${res.messages.error}`);
                return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
            }
        } catch (error) {
            console.log('error:', error);
            await ctxFn.flowDynamic(`Error al consultar la Solicitud de requisicion #${reqSelected}.`);
            return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
        }

    })
    .addAction({ capture: true }, async (ctx, ctxFn) => {
        const reqLinSelected = ctxFn.state.get('reqLinSelected');
        const reqSelected = ctxFn.state.get('reqSelected');
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');

        if (isNaN(ctx?.body)) {
            return await ctxFn.fallBack();
        }
        //si el número digitado es 0, salimos del flujo
        if (parseInt(ctx.body) === 0) {
            return await ctxFn.gotoFlow(flowRequisionDet);
        }

        if (parseInt(ctx.body) == 1) {
            try {
                const res = await aprRequisitionLine(reqSelected, reqLinSelected, uid, token);

                if (res.code === 200) {
                    await ctxFn.flowDynamic(`la Solicitud de requisicion #${reqSelected} y Linea  #${reqLinSelected} ha sido aprobada Exitosmante.`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                } else {
                    await ctxFn.flowDynamic(`${res.messages.error}`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                }
            } catch (error) {
                console.log('error:', error);
                await ctxFn.flowDynamic(`Error al aprobar la Solicitud de requisicion #${reqSelected} y Linea  #${reqLinSelected}.`);
                return await ctxFn.gotoFlow(flowRequisionFin);
            }
        }
        if (parseInt(ctx.body) == 2) {

            try {
                const res = await rechRequisitionLine(reqSelected, reqLinSelected, uid, token);

                if (res.code === 200) {
                    await ctxFn.flowDynamic(`la Solicitud de requisicion #${reqSelected} y Linea  #${reqLinSelected} ha sido rechazada Exitosmante.`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                } else {
                    await ctxFn.flowDynamic(`${res.messages.error}`);
                    return await ctxFn.gotoFlow(flowRequisionFin);
                }
            } catch (error) {
                console.log('error:', error);
                await ctxFn.flowDynamic(`Error al rechazar la Solicitud de requisicion #${reqSelected} y Linea  #${reqLinSelected}.`);
                return await ctxFn.gotoFlow(flowRequisionFin);
            }
        }
        if (parseInt(ctx.body) > 2) {
            return await ctxFn.fallBack();
        }

    });
export { flowRequisionLineDet };
