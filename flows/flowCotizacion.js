/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowMenu } from "./flowMenu.js";
import { flowCotizacionDet } from "./flowCotizacionDet.js";
/**
 * Funciones
 */
import { getOpenCotizations } from "../api/woomi.service.js";
import { numeroAEmoji } from "../api/utils.js";

/**
 * Constantes Flow
 */
const flowCotizacion = bot.addKeyword(bot.EVENTS.ACTION)
  .addAnswer(['Estamos procesando su solicitud, por favor espere....'])
  .addAction(async (_, ctxFn) => {
    const identification_number = parseFloat(ctxFn.state.get('identification_number')) || 0;
    const uid = ctxFn.state.get('uid');
    const token = ctxFn.state.get('token');

    try {
      const res = await getOpenCotizations(identification_number, uid, token);
      if (res.code === 200) {
        await ctxFn.state.update({
          cotItems: res.data,
        });
        let dataIt = res.data || [];
        //console.log('res:', res);
        let listItems = dataIt.map((item, index) =>
          `${numeroAEmoji(index + 1)} Cot: ${item.number}, Detalle: ${item.description}`
        ).join('\n');
        listItems = `${listItems}\n${numeroAEmoji(0)} ⬅️ ⁠Volver al Menú anterior`;
        return await ctxFn.flowDynamic(`*Listado de Cotizaciones:*\n${listItems}\n\n\n*para ver el detalle digite una opción:*`);
      } else if (res.code === 404) {
        await ctxFn.flowDynamic(`No se encontraron cotizaciones abiertas para este usuario, por favor intenta más tarde.`);
        return await ctxFn.flowDynamic(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
      } else {
        await ctxFn.flowDynamic(`${res.messages.error}`);
        return await ctxFn.flowDynamic(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
      }
    } catch (error) {
      console.log('error:', error);
      await ctxFn.flowDynamic(`Error al consultar las cotizaciones abiertas para el usuario.`);
      return await ctxFn.flowDynamic(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);

    }


  })
  .addAction({ capture: true }, async (ctx, ctxFn) => {
    if (isNaN(ctx?.body)) {
      await ctxFn.state.update({
        cotItems: [],
      });
      return await ctxFn.gotoFlow(flowCotizacion);
    }
    //si el número digitado es 0, salimos del flujo
    if (parseInt(ctx.body) === 0) {
      await ctxFn.state.update({
        cotItems: [],
      });
      return await ctxFn.gotoFlow(flowMenu);
    }
    //validamos que el número digitado sea correcto y que exista en el listado
    const cotItems = ctxFn.state.get('cotItems');
    const item = cotItems[parseInt(ctx.body) - 1];
    if (!item) {
      await ctxFn.state.update({
        cotItems: [],
      });
      ctxFn.flowDynamic(`El número digitado no es válido, por favor intenta nuevamente.`);
      return await ctxFn.gotoFlow(flowCotizacion);
    }

    await ctxFn.state.update({
      cotSelected: item.number,
    });
    //const myState = ctxFn.state.getMyState();
    //console.log('myState:', myState);
    return ctxFn.gotoFlow(flowCotizacionDet);
  });
export { flowCotizacion };
