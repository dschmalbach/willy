/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowFacturaDet } from "./flowFacturaDet.js";
import { flowFacturaFin } from "./flowFacturaFin.js";
/**
 * Funciones
 */
import { numeroAEmoji } from "../api/utils.js";
import { aprInvoice } from "../api/woomi.service.js";
/**
 * Constantes Flow
 */
const flowFacturaRateDet = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer([
        "*Seleccione Motivo:*",
        `${numeroAEmoji(1)} MUY SATISFECHO`,
        `${numeroAEmoji(2)} NO CONFORME`,
        `${numeroAEmoji(3)} SATISFECHO`,
        `${numeroAEmoji(0)} ⬅️ Cancelar y volver al detalle de la factura`,
    ], { capture: true }, async (ctx, ctxFn) => {

        const invItemSelected = ctxFn.state.get('invItemSelected');
        const invRate = ctxFn.state.get('invRate');

        if (isNaN(ctx?.body)) {
            return await ctxFn.fallBack();
        }

        if (parseInt(ctx.body) === 0) {
            return await ctxFn.gotoFlow(flowFacturaDet);
        }

        if (parseInt(ctx?.body) > 4) {
            return await ctxFn.fallBack();
        }

        let flowDynamic_ = '';
        let invComment = '';

        switch (parseInt(ctx.body)) {
            case 1:
                flowDynamic_ = `Confirmamos que la factura #${invItemSelected.number}. ha sido Aprobada con una calificación de: *${invRate}* motivo: *MUY SATISFECHO*`;
                invComment = 'MUY SATISFECHO';
                break;
            case 2:
                flowDynamic_ = `Confirmamos que la factura #${invItemSelected.number}. ha sido Aprobada con una calificación de: *${invRate}* motivo: *NO CONFORME*`;
                invComment = 'NO CONFORME';
                break;
            case 3:
                flowDynamic_ = `Confirmamos que la factura #${invItemSelected.number}. ha sido Aprobada con una calificación de: *${invRate}* motivo: *SATISFECHO*`;
                invComment = 'SATISFECHO';
                break;
            default:
                return await ctxFn.fallBack();
        }

        await ctxFn.state.update({
            invComment: invComment,
        });

        const costNumber = ctxFn.state.get('costNumber');
        const userName = ctxFn.state.get('username'); // "sistemas";//
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');

        try {
            const res = await aprInvoice(invItemSelected.id, invItemSelected.number, userName, costNumber, "", invRate, invComment, invItemSelected.numoc, invItemSelected.provider, invItemSelected.typeasoc, uid, token);

            if (res.code === 200) {
                await ctxFn.flowDynamic(flowDynamic_);
            } else {
                await ctxFn.flowDynamic(`${res.messages.error}`);
            }
            return await ctxFn.gotoFlow(flowFacturaFin);
        } catch (error) {
            console.log(error);
            await ctxFn.flowDynamic(`Error al aprobar la Solicitud de factura #${invItemSelected.number}.`);
            return await ctxFn.gotoFlow(flowFacturaFin);
        }
    });
export { flowFacturaRateDet };