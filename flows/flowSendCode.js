/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowAuth } from "./flowAuth.js";
/**
 * Funciones
 */
import { sendkey } from "../api/woomi.service.js";
/**
 * Constantes Flow
 */
const flowSendCode = bot.addKeyword(bot.EVENTS.ACTION)
  .addAnswer(
    "Por favor digite la cédula sin puntos ni comas para continuar.",
    {
      capture: true,
    },
    async (ctx, ctxFn) => {
      //validar cedula numerica y que tenga 6 digitos o mas
      if (isNaN(ctx?.body) || ctx?.body.length < 6) {
        //console.log('Cédula:', ctx.body);
        return await ctxFn.fallBack();
      }

      try {
        let res = await sendkey(parseInt(ctx.body));
        // console.log('res:', res);
        if (res.code === 200) {
          await ctxFn.state.update({
            identification_number: ctx.body,
            auth: res.data.auth,
            uid: res.data.uid,
            send: res.data.valor,
            type: res.data.type,
          });

          const send = ctxFn.state.get('send');
          const type = ctxFn.state.get('type');
          let typeAuth = type === "mail" ? "correo" : "celular";

          await ctxFn.flowDynamic(`Hemos enviado un código de verificación al ${typeAuth} ${send}`)
          return ctxFn.gotoFlow(flowAuth);
        } else {
          await ctxFn.state.clear();
          await ctxFn.flowDynamic(`${res.messages.error}`);
          return await ctxFn.fallBack();
        }
      } catch (error) {
        console.log('error:', error);
        await ctxFn.state.clear();
        await ctxFn.flowDynamic(`Error al enviar el código de verificación.`);
        return await ctxFn.fallBack();
      }

    });
export { flowSendCode };