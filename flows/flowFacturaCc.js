/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowFacturaRate } from "./flowFacturaRate.js";
/**
 * Funciones
 */
import { getCostCenterInfo } from "../api/woomi.service.js";
/**
 * Constantes Flow
 */

const flowFacturaCc = bot.addKeyword(bot.EVENTS.ACTION)
  .addAnswer(
    "Por favor digite el número del centro de costos.",
    {
      capture: true,
    },
    async (ctx, ctxFn) => {
      //validar cedula numerica y que tenga solo 4 digitos
      if (isNaN(ctx?.body) && ctx?.body.length < 4 || ctx?.body.length > 4) {
        //console.log('centro de costos:', ctx.body);
        return await ctxFn.fallBack();

      }
      try {
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');
        const res = await getCostCenterInfo(parseInt(ctx.body), uid, token);
        if (res.code === 200) {
          await ctxFn.flowDynamic(`${res.data.description}.`);
          await ctxFn.state.update({
            costNumber: parseInt(ctx.body),
          });

          return await ctxFn.gotoFlow(flowFacturaRate);
        } else {
          const nNumC = parseInt(ctxFn.state.get('nNumC')) || 0;
          if (nNumC >= 3) {
            return await ctxFn.flowDynamic(`Has superado el número de intentos permitidos, por favor intenta más tarde.`);
          } else {
            await ctxFn.flowDynamic(`${res.messages.error}`);
            await ctxFn.state.update({
              nNumC: nNumC + 1,
            });
            return await ctxFn.fallBack();
          }
        }
      } catch (error) {
        console.log('error:', error);
        await ctxFn.flowDynamic("Error al consultar el centro de costos.");
        return await ctxFn.fallBack();
      }

    });
export { flowFacturaCc };