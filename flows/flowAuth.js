/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";

/**
 * Flows
 */
import { flowMenu } from "./flowMenu.js";
import { flowInactividad } from "./flowInactividad.js";

/**
 * Funciones
 */
import { login } from "../api/woomi.service.js";

/**
 * Constantes Flow
 */
const flowAuth = bot.addKeyword(bot.EVENTS.ACTION)
  .addAnswer(
    "por favor digita el código para continuar.",
    {
      capture: true,
    },
    async (ctx, ctxFn) => {
      if (isNaN(ctx?.body) || ctx?.body.length < 6) {
        //console.log('Cédula:', ctx.body);
        return await ctxFn.fallBack();
      }
      const uid = ctxFn.state.get('uid');
      let res = await login(uid, parseInt(ctx.body));
      //console.log('res:', res);
      if (res.code === 200) {
        await ctxFn.state.update({
          token: res.data.token,
          picture: res.data.picture,
          username: res.data.username,
          nombre: res.data.nombre,
          phone: res.data.phone,
          reqItems: [],
          cotItems: [],
        });
        await ctxFn.flowDynamic(`Bienvenido *${res.data.nombre}*`);
        return ctxFn.gotoFlow(flowMenu);
      } else {
        const nNum = parseInt(ctxFn.state.get('nNum')) || 0;
        if (nNum >= 3) {
          await ctxFn.state.clear();
          return await ctxFn.flowDynamic(`Has superado el número de intentos permitidos, por favor intenta más tarde.`);
        } else {
          await ctxFn.flowDynamic(`${res.messages.error}`);
          await ctxFn.state.update({
            nNum: nNum + 1,
          });
          return await ctxFn.fallBack();
        }
      }
    });
export { flowAuth };