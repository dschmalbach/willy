/**
 * Librerias
 */
import bot from "@bot-whatsapp/bot";
/**
 * Flows
 */
import { flowFactura } from "./flowFactura.js";
import { flowFacturaRech } from "./flowFacturaRech.js";
import { flowFacturaCc } from "./flowFacturaCc.js";
/**
 * Funciones
 */
import { getInvoiceDetails, aprCotizations } from "../api/woomi.service.js";
import { formatInvoicesDetails, numeroAEmoji } from "../api/utils.js";
/**
 * Constantes Flow
 */
const flowFacturaDet = bot.addKeyword(bot.EVENTS.ACTION)
    .addAnswer('procesando....')
    .addAction(async (_, ctxFn) => {
        const invItemSelected = ctxFn.state.get('invItemSelected');
        const uid = ctxFn.state.get('uid');
        const token = ctxFn.state.get('token');
        try {
            const res = await getInvoiceDetails(invItemSelected.id, invItemSelected.number, uid, token);
            if (res.code === 200) {
                //console.log('data:', res.data);
                let msm = await formatInvoicesDetails(res.data);
                return await ctxFn.flowDynamic(msm)
            } else {
                await ctxFn.flowDynamic(`${res.messages.error}`);
                return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
            }
        } catch (error) {
            console.log(error);
            await ctxFn.flowDynamic(`Error al consultar la Solicitud de cotizacion #${invSelected}.`);
            return await ctxFn.gotoFlow(`${numeroAEmoji(0)}. ⬅️ ⁠Volver al Menú anterior`);
        }

    })
    .addAction({ capture: true }, async (ctx, ctxFn) => {
        const invItemSelected = ctxFn.state.get('invItemSelected');
        if (isNaN(ctx?.body)) {
            return await ctxFn.gotoFlow(flowFacturaDet);
        }
        //si el número digitado es 0, salimos del flujo
        if (parseInt(ctx.body) === 0) {
            return await ctxFn.gotoFlow(flowFactura);
        }

        if (parseInt(ctx.body) === 1) {
            await ctxFn.flowDynamic(`Usted ha decidido aprobar la Solicitud de factura #${invItemSelected.number}.`);
            return await ctxFn.gotoFlow(flowFacturaCc);
        }

        if (parseInt(ctx.body) === 2) {
            await ctxFn.flowDynamic(`Usted ha decidido rechazar la factura #${invItemSelected.number}.`);
            return await ctxFn.gotoFlow(flowFacturaRech);
        }
        if (parseInt(ctx.body) > 2) {
            return await ctxFn.gotoFlow(flowFacturaDet);
        }
    });
export { flowFacturaDet };