///EABEPaD17ZAuUBABE0qH5vKoZC0Fgr9KUZAASgE0Vn62ZCZAA5SDCb2fJ4U43C5WZCI9QkT6wipWgXqTll9wcuJMXmum2Y22sxRZCNbZBWQ5p4VCDdGBbrFFBs51dx2geadowEvtUXr42UBDinpd19AcRIjJKKoBKlOi2hTJJ5P77Sqy4lP3BZAjUhDdVAeETL91C1ILutFcCTnAZDZD

import "dotenv/config";
import bot from "@bot-whatsapp/bot";
import QRPortalWeb from "@bot-whatsapp/portal";
import BaileysProvider from "@bot-whatsapp/provider/baileys";
import MetaProvider from "@bot-whatsapp/provider/meta";
import MockAdapter from "@bot-whatsapp/database/mock";
//import JsonFileAdapter from "@bot-whatsapp/database/json";

/**
 * Flows
 */
import { flowPrincipal } from "./flows/flowPrincipal.js";
import { flowSendCode } from "./flows/flowSendCode.js";
import { flowAuth } from "./flows/flowAuth.js";
import { flowMenu } from "./flows/flowMenu.js";
import { flowInactividad } from "./flows/flowInactividad.js";
import { flowFinalizar } from "./flows/flowFinalizar.js";

import { flowCotizacion } from "./flows/flowCotizacion.js";
import { flowCotizacionDet } from "./flows/flowCotizacionDet.js";
import { flowCotizacionFin } from "./flows/flowCotizacionFin.js";
import { flowCotizacionRech } from "./flows/flowCotizacionRech.js";

import { flowRequision } from "./flows/flowRequision.js";
import { flowRequisionDet } from "./flows/flowRequisionDet.js";
import { flowRequisionLineDet } from "./flows/flowRequisionLineDet.js";
import { flowRequisionFin } from "./flows/flowRequisionFin.js";

import { flowFactura } from "./flows/flowFactura.js";
import { flowFacturaDet } from "./flows/flowFacturaDet.js";
import { flowFacturaCc } from "./flows/flowFacturaCc.js";
import { flowFacturaFin } from "./flows/flowFacturaFin.js";
import { flowFacturaRech } from "./flows/flowFacturaRech.js";
import { flowFacturaRechDet } from "./flows/flowFacturaRechDet.js";
import { flowFacturaRate } from "./flows/flowFacturaRate.js";
import { flowFacturaRateDet } from "./flows/flowFacturaRateDet.js";

import { flowOc } from "./flows/flowOc.js";
/**
 * Funcion principal
 */
const main = async () => {
  const adapterDB = new MockAdapter();
  //const adapterDB = new JsonFileAdapter();

  const adapterFlow = bot.createFlow([
    flowPrincipal,
    flowSendCode,
    flowAuth,
    flowMenu,
    flowFinalizar,
    flowInactividad,
    flowCotizacion,
    flowCotizacionDet,
    flowCotizacionFin,
    flowCotizacionRech,
    flowRequision,
    flowRequisionDet,
    flowRequisionLineDet,
    flowRequisionFin,
    flowFactura,
    flowFacturaDet,
    flowFacturaCc,
    flowFacturaFin,
    flowFacturaRech,
    flowFacturaRechDet,
    flowFacturaRate,
    flowFacturaRateDet,
    flowOc
  ]);

  //const adapterProvider = bot.createProvider(BaileysProvider);

  const adapterProvider = bot.createProvider(MetaProvider, {
    jwtToken: 'EAAPNFrJ8yEUBO3ZByDqMLrV7zNv7gzjdfZBZBYc3f05TNDAnq5m1MjQbZBciMAHaH2pLZByg56YRoxfxZBRaNVwXfBp8mNBfUw5tlvxFXbnTJDTZBEZBSZCl0nw4w2yWamaRRx6hR8HGj9vFj3LTbfKK51XXoVsV5ZBxMtRpwZCp7CK5XYqI76xBahtptZAsFybmti8M',
    numberId: '249036751616474',
    verifyToken: 'dschmalbach',
    version: 'v19.0',
  })


  const configBot = {
    flow: adapterFlow,
    provider: adapterProvider,
    database: adapterDB,
  }
  const configExtra = {
    extensions: {}
  }
  bot.createBot(configBot, configExtra);

  //QRPortalWeb();
};
main();